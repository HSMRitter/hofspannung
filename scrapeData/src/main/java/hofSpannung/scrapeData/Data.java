package hofSpannung.scrapeData;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Data {

	public ArrayList<String[]> getMatchesByUrl(String url)
	{
		 Document doc = null;
		try
		{
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			System.out.println("Connection Error (createDoc() JSoup.coonect)");
			System.exit(0);
		}
				
		ArrayList<String[]> test = new ArrayList<String[]>();
		
		try
		{
			Element table = doc.select("table").get(0);
			Elements rows = table.select("tr");
			String date = "-";
		
			for (int i = 1; i < rows.size(); i++)
			{
				String[] match = new String[4];
				Element row = rows.get(i);
			    Elements cols = row.select("td");	    
			    
			    if(!cols.get(0).text().equals(""))
			    {
				    match[0] = cols.get(0).text();
				    date = match[0];
			    }
			    else
			    {
			    	match[0] = date;
			    }
			    	
			    if(cols.get(3).text().contains("TV Ko") || cols.get(4).text().contains("TV Ko"))
			    {
			    	 match[1] = cols.get(3).text();
				 match[2] = cols.get(4).text();
			    }
			}
		}
		catch (Exception e) {
			System.out.println("Scraping Error (createDoc()) at " + url);
			System.exit(0);
		}
		
		return null;
	}
}
